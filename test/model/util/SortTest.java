package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import model.util.Sort;

public class SortTest extends TestCase{

	// Muestra de datos a ordenar
	private Comparable[] datos;

	@Before
	public void setUp() throws Exception{

		datos = new Comparable[5];
		datos[0] = "z";
		datos[1]= "a";
		datos[2]= "c";
		datos[3]= "d"; 
		datos[4] = "e";
	}

	public void testQuickSort()
	{
		Sort.ordenarQuickSort(datos);
		assertEquals("Error al ordenar por QuickSort","a",datos[0] );
		assertEquals("Error al ordenar por QuickSort","c" ,datos[1] );
		assertEquals("Error al ordenar por QuickSort","d" ,datos[2] );
		assertEquals("Error al ordenar por QuickSort", "e",datos[3] );
		assertEquals("Error al ordenar por QuickSort", "z",datos[4] );
	}

	public void testMergeSort() 
	{
		datos=new Comparable[5];
		datos[0] = 1;
		datos[1]= 3;
		datos[2]= 4;
		datos[3]= 2; 
		datos[4] = 0;
		Sort.sortM(datos);
		assertEquals("Error al ordenar por MergeSort",0,datos[0] );
		assertEquals("Error al ordenar por MergeSort",1,datos[1] );
		assertEquals("Error al ordenar por MergeSort",2,datos[2] );
		assertEquals("Error al ordenar por MergeSort",3,datos[3] );
		assertEquals("Error al ordenar por MergeSort",4,datos[4] );
	}


	public void testShellSort()
	{
		datos=new Comparable[5];
		datos[0] = "a";
		datos[1]= "z";
		datos[2]= "x";
		datos[3]= "y"; 
		datos[4] = "m";
		Sort.ordenarShellSort(datos);
		System.out.println(datos);
		assertEquals("Error al ordenar por ShellSort","a",datos[0] );
		assertEquals("Error al ordenar por ShellSort","m",datos[1] );
		assertEquals("Error al ordenar por ShellSort","x" ,datos[2] );
		assertEquals("Error al ordenar por ShellSort","y",datos[3] );
		assertEquals("Error al ordenar por ShellSort","z",datos[4] );
	}


}
