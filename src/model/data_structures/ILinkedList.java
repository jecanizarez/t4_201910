package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	T getElement (int num); 
		
	public boolean a�adirFinal(T dato); 
	
	public boolean a�adirAt( int num, T dato ); 
	
	public void eliminarUlt(); 
	
	public boolean EliminarAt(int num);
	
	T getCurrentElement(); 
}
