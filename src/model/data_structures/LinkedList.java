package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T>{


	private NodoLista<T> primero; 

	private NodoLista<T> ultimo;

	private int peso; 

	private NodoLista<T> actual;

	public LinkedList()
	{
		primero = null; 
		ultimo = null; 	
		actual = null;
		peso = 0;
	}

	@Override
	/**
	 * A�ade un elemento a la lista encadenada segun la posicion
	 * @num numero que indica la posicion donde se va a agregar el objeto
	 * @dato dato que se va a agregar a la lista
	 * @return Retorna false en el caso en que el indice donde se quiere agregar es no existe
	 */
	public boolean a�adirAt(int num, T dato) {
		NodoLista<T> nuevo = new NodoLista<T>(dato);
		int indice = 0; 
		if( num > peso)
		{
			return false;
		}
		actual = primero;
		while(actual != null && indice != num-1)
		{
			actual = actual.darSiguiente();
			indice++;
		}
		if(actual.darSiguiente() != null)
		{
			nuevo.cambiarSiguiente(actual.darSiguiente());
			actual.darSiguiente().cambiarAnterior(nuevo);
			actual.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(actual);
			peso++;
			return true;
		}
		else
		{
			actual.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(actual);
			peso++;
			return true;
		}

	}

	@Override
	public boolean EliminarAt(int num) 
	{
		actual = primero;
		int indice = 0;
		if(num > peso)
		{
			return false;
		}
		else if(num == 0)
		{
			if(primero != null && primero.darSiguiente() == null)
			{
				primero = null;
				peso--;
			}
			else if(primero.darSiguiente()!= null)
			{
				primero.darSiguiente().cambiarAnterior(null);
				primero = primero.darSiguiente();
				peso--;
			}
		}
		else if(num == peso-1)
		{
			eliminarUlt();
			return true;
		}
		while(actual != null && indice != num-1)
		{
			actual = actual.darSiguiente();
			indice++;
		}
		if(actual != null && actual.darSiguiente() != null && actual.darSiguiente().darSiguiente() != null)
		{
			actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
			actual.darSiguiente().darSiguiente().cambiarAnterior(actual);
			peso--;
			return true;
		}
		return false;
	}


	public boolean a�adirFinal(T dato)
	{
		NodoLista<T> nuevo = new NodoLista<T>(dato);
		if(primero == null)
		{
			actual = nuevo;
			primero = nuevo; 
			ultimo = nuevo;
			peso++;
			return true; 
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);	
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
			peso++;
			return true;
		}

	}
	public void eliminarUlt()
	{
		NodoLista<T> previous = ultimo.darAnterior();
		previous.cambiarSiguiente(null);
		peso--;
	}
	public Integer getSize()
	{
		return peso;
	}
	public Iterator<T> iterator()
	{

		return new IteratorLista<>(primero);
	}

	public T getCurrentElement() {

		return actual.darElemento();
	}

	public T getElement(int num) 
	{
		int indice = 0;
		if(num >= peso)
		{
			return null;
		}
		actual = primero;
		while(actual != null && indice < num)
		{
			actual = actual.darSiguiente();
			indice++;
		}
		return actual.darElemento();
	}


	private static class NodoLista<T> 
	{
		private T elemento;

		private NodoLista<T> siguiente;

		private NodoLista<T> anterior; 


		public NodoLista(T dato)
		{
			elemento = dato;
			siguiente = null;
			anterior = null;
		}

		public void cambiarSiguiente(NodoLista<T> dato)
		{
			siguiente = dato;
		}
		public NodoLista<T> darSiguiente()
		{
			return siguiente;
		}
		public T darElemento()
		{
			return elemento;
		}
		public void cambiarAnterior(NodoLista<T> dato)
		{
			anterior = dato;	
		}
		public NodoLista<T> darAnterior()

		{
			return anterior;
		}

	} 	
	
	private static class IteratorLista<T> implements Iterator<T>
	{
		private NodoLista<T> proximo; 

		private NodoLista<T> ant_prox; 

		private NodoLista<T> ant_ant; 

		public IteratorLista(NodoLista<T> primero)

		{
			proximo = primero; 
		}
		@Override
		public boolean hasNext() {
			return proximo != null; 
		}

		@Override
		public T next() 
		{
			T retornar = proximo.darElemento();
			ant_prox = proximo;
			ant_ant = proximo.darAnterior();
			proximo = proximo.darSiguiente(); 
			return retornar; 
		}

	}
}
