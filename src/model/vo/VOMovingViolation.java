package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	private int objectId;
	private String location;
	private int totalPaid;
	private String accidentIndicator;
	private String ticketIssueDate;
	private String violationDesc;
	private String violationCode;


	public VOMovingViolation(int pId, String pLocation, String pTicketIssueDate, int pTotalPaid, String pAccidentIndicator, String pViolationDescription,String pViolationCode)
	{
		objectId=pId;
		location=pLocation;
		ticketIssueDate=pTicketIssueDate;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		violationDesc=pViolationDescription;
		violationCode=pViolationCode;

	}
	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public int getObjectId() 
	{
		return objectId;
	}	


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() {
		return violationDesc;
	}

	public String getViolationCode()
	{
		return violationCode;
	}


	public int compareTo(VOMovingViolation cmp) 
	{
		if(ticketIssueDate.compareTo(cmp.getTicketIssueDate())==0)
		{
			return (objectId-cmp.getObjectId());
		}
		else return ticketIssueDate.compareTo(cmp.getTicketIssueDate());
	}

	public String toString()
	{
		return "Object ID: "+ objectId+" Location: "+location+" Ticket Issue Date: "+ticketIssueDate+" Total Paid: "+totalPaid+" Accident Indicator: "+ accidentIndicator+" Violation Description: "+violationDesc+" Violation Code: "+violationCode;
	}

}
