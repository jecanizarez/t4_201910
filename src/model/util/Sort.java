package model.util;
import java.util.Random;
import org.hamcrest.StringDescription;

import edu.princeton.cs.introcs.StdRandom;
public class Sort {

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) 
	{
		int n=datos.length;
		int h=1;
		while(h<n/3)
		{
			h=3*h+1;
		}
		while(h>=1)
		{
			for (int i=h;i<n;i++)
			{
				for(int j=i;j>=h && less(datos[j],datos[j-h]);j-=h)
					exchange(datos,j,j-h);		
			}
			h=h/3;
		}

	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 * @param hi 
	 * @param mid 
	 * @param lo 
	 * @param aux 
	 */
	public static void ordenarMergeSort( Comparable[ ] datos, Comparable[] aux, int lo, int mid, int hi ) 
	{
		for (int k = lo; k <= hi; k++)
		{	
			aux[k] = datos[k]; 
		}
		int i = lo;
		int j = mid+1;

		for (int k = lo; k <= hi; k++)
		{
			if      (i > mid)            
				datos[k] = aux[j++];
			else if (j > hi)             
				datos[k] = aux[i++];
			else if (less(aux[j], aux[i])) 
				datos[k] = aux[j++];
			else                          
				datos[k] = aux[i++];
		}
	}
	private static void sortM(Comparable[]datos, Comparable[] aux, int lo, int hi)
	{
		if (hi<=lo)
			return;
		int mid=lo+(hi-lo)/2;
		sortM(datos,aux,lo,mid);
		sortM(datos,aux,mid+1,hi);
		ordenarMergeSort(datos,aux,lo,mid,hi);
	}

	public static void sortM(Comparable[]datos)
	{
		Comparable[]aux=new Comparable[datos.length];
		sortM(datos,aux,0,datos.length-1);
	}
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo QuickSort

		StdRandom.shuffle(datos);
		sortQ(datos, 0, datos.length-1);
	}
	private static void sortQ(Comparable[]datos, int lo, int hi)
	{
		if ( hi <= lo)
		{
			return;
		}
		int j = partition(datos, lo, hi);
		sortQ(datos, lo, j-1);
		sortQ(datos, j+1, hi);
	}


	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 *param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		return v.compareTo(w)<0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar	
		Comparable temp = datos[i]; 
		datos[i] = datos[j]; 
		datos[j] = temp;
	}
	private static int partition(Comparable[] datos, int lo, int hi)
	{
		int i = lo; 
		int j = hi+1;
		while(true)
		{
			while(less(datos[++i], datos[lo]))
			{
				if(i == hi)
				{
					break; 
				}
			}
			while(less(datos[lo], datos[--j]))
			{
				if(j == lo)
				{
					break;
				}
			}
			if( i>=j)
			{
				break;
			}
			exchange(datos, i, j);
		}
		exchange(datos, lo, j);
		return j;
	}

}
